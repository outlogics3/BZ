const express = require('express')
const app = express()
var bodyParser = require('body-parser')
const port = 3001
const userRoute = require('./routes/user.routes')
const bookRoute = require('./routes/book.routes')
const genreRoute = require('./routes/genre.routes')
const chatRoomRouter =require("./routes/chatroom.routes");
const deleteRouter =require("./routes/delete.route");

//import chatRoomRouter from "./routes/chatroom.routes";
//import deleteRouter from "./routes/delete.route";

const mongoose = require('mongoose')

const socketio = require("socket.io");
const WebSockets =("./utils/WebSockets");


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use('/users',userRoute)
app.use('/books',bookRoute)
app.use('/genres',genreRoute)
app.use("/room", chatRoomRouter);
app.use("/delete", deleteRouter);

mongoose.connect('mongodb+srv://book_arrow:book_arrow@cluster0.x5gnu.mongodb.net/test')
    .then(() => {
        console.log('Database connections succesful')
    }
    )
    .catch(err => {
        console.error(err)
        console.error('Database connection error')
    }
    )

    const server = http.createServer(app);
/** Create socket connection */
global.io = socketio.listen(server);
global.io.on('connection', WebSockets.connection)
server.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})