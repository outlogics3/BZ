const express = require("express");
const router = express.Router();
const genreController=require("../src/genre/genre.controller")
const validatetoken =require("../src/helpers/validateToeken")
//router.param("genreById",genreController.getGenreById)
router.post("/creategen",validatetoken.validateToken, genreController.createGenre)
router.put("/:id/update",validatetoken.validateToken, genreController.updateGenre);
router.delete("/:id/delete", validatetoken.validateToken,genreController.deleteGenre);

module.exports = router;
