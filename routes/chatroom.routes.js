const express = require("express");
const router = express.Router();
const chatRoom= require("../src/chat/chat.controller")
const validatetoken =require("../src/helpers/validateToeken")

router
  .get('/', validatetoken.validateToken,chatRoom.getRecentConversation)
  .get('/:roomId', validatetoken.validateToken,chatRoom.getConversationByRoomId)
  .post('/initiate',validatetoken.validateToken, chatRoom.initiate)
  .post('/:roomId/message',validatetoken.validateToken, chatRoom.postMessage)
  .put('/:roomId/mark-read',validatetoken.validateToken, chatRoom.markConversationReadByRoomId)

export default router;